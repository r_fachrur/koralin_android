package id.koralin.android.labtek_indie.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


public class Skills implements Parcelable {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static class DataBean {
        /**
         * id_freelancer : 1
         * nama : Rafly
         * umur : 20
         * alamat : Talun
         * deskripsi : Lonely Wolf
         * link_image : -
         * email : flashzapper@gmail.com
         * social_fb : www.facebook.com/rafly.flashzapper
         * social_linkedin : www.linkedin.com
         * portofolio : https://steamcommunity.com/id/flyflash
         * status_kerja : 1
         * daily_rate : 999999
         */

        private int id_freelancer;
        private String nama;
        private int umur;
        private String alamat;
        private String deskripsi;
        private String link_image;
        private String email;
        private String social_fb;
        private String social_linkedin;
        private String portofolio;
        private String status_kerja;
        private String daily_rate;

        public int getId_freelancer() {
            return id_freelancer;
        }

        public void setId_freelancer(int id_freelancer) {
            this.id_freelancer = id_freelancer;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public int getUmur() {
            return umur;
        }

        public void setUmur(int umur) {
            this.umur = umur;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getLink_image() {
            return link_image;
        }

        public void setLink_image(String link_image) {
            this.link_image = link_image;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSocial_fb() {
            return social_fb;
        }

        public void setSocial_fb(String social_fb) {
            this.social_fb = social_fb;
        }

        public String getSocial_linkedin() {
            return social_linkedin;
        }

        public void setSocial_linkedin(String social_linkedin) {
            this.social_linkedin = social_linkedin;
        }

        public String getPortofolio() {
            return portofolio;
        }

        public void setPortofolio(String portofolio) {
            this.portofolio = portofolio;
        }

        public String getStatus_kerja() {
            return status_kerja;
        }

        public void setStatus_kerja(String status_kerja) {
            this.status_kerja = status_kerja;
        }

        public String getDaily_rate() {
            return daily_rate;
        }

        public void setDaily_rate(String daily_rate) {
            this.daily_rate = daily_rate;
        }
    }
}
