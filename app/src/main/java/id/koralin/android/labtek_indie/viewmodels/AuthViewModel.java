package id.koralin.android.labtek_indie.viewmodels;

import id.koralin.android.labtek_indie.data.DataManager;

/**
 * Created by rfachrur on 02/10/17.
 *
 */

public class AuthViewModel extends BaseViewModel
{
    String userId, password;

    public void login ()
    {
        DataManager.login(userId, password);
    }
}
