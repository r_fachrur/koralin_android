package id.koralin.android.labtek_indie.utils;

import java.util.ArrayList;
import java.util.List;

import id.koralin.android.labtek_indie.models.User;

/**
 * Created by rfachrur on 02/11/17.
 *
 */

public class DummyDataFactory
{
    public static List<User> createDummyUsers ()
    {
        List<User> lUser = new ArrayList<>();
        for (int i = 0; i < 5; i++)
        {
            lUser.add(new User(i, String.format("nama user %d", i)));
        }
        return lUser;
    }
}
