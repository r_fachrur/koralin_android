package id.koralin.android.labtek_indie.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rfachrur on 02/10/17.
 *
 */

public class User
{
    @SerializedName ("id")
    public int    id;
    @SerializedName ("name")
    public String name;

    public User (int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId ()
    {
        return id;
    }

    public String getName ()
    {
        return name;
    }
}
