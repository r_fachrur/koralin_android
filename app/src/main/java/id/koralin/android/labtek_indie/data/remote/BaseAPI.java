package id.koralin.android.labtek_indie.data.remote;

import org.greenrobot.eventbus.EventBus;

import id.koralin.android.labtek_indie.MyApplication;
import id.koralin.android.labtek_indie.data.events.BaseEvent;
import id.koralin.android.labtek_indie.data.events.EmptyEvent;
import id.koralin.android.labtek_indie.data.events.ErrorEvent;
import id.koralin.android.labtek_indie.utils.constants.I;
import retrofit2.Response;

/**
 * Created by rfachrur on 02/10/17.
 *
 */

abstract class BaseAPI
{
    MyApplication app   = MyApplication.getInstance();
    EventBus      event = EventBus.getDefault();

    EmptyEvent mEmptyEvent = new EmptyEvent();

    void handleResponse (Response response, BaseEvent eventClass)
    {
        if (response.isSuccessful())
        {
            if (response.code() == I.HTTP_NO_CONTENT)
            {
                event.post(mEmptyEvent);
            }
            else
            {
                event.post(eventClass);
            }
        }
        else
        {
            event.post(new ErrorEvent(response));
        }
    }
}
