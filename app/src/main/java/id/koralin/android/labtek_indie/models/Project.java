package id.koralin.android.labtek_indie.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Project implements Parcelable {

    @SerializedName("id_project")
//    private int id;
    private String id;

    @SerializedName("judul")
    private String title;

    @SerializedName("deskripsi")
    private String description;

    @SerializedName("total_sprint")
    private String totalSprint;
    @SerializedName("durasi")
    private String duration;
    @SerializedName("komposisi_id")
    private String compositionID;
    @SerializedName("jenis")
    private String projectType;
    @SerializedName("client")
    private String client;
    @SerializedName("status_project")
    private String projectStatus;


    public Project(String id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Project(String id, String title, String description, String totalSprint, String duration, String compositionID, String projectType, String client, String projectStatus) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.totalSprint = totalSprint;
        this.duration = duration;
        this.compositionID = compositionID;
        this.projectType = projectType;
        this.client = client;
        this.projectStatus = projectStatus;
    }

    public static ArrayList<Project> getItems() {
        ArrayList<Project> items = new ArrayList<>();
        items.add(new Project("1", "Title-1", "Description-1"));
        items.add(new Project("2", "Title-2", "Description-2"));
        items.add(new Project("3", "Title-3", "Description-3"));
        items.add(new Project("4", "Title-4", "Description-4"));
        items.add(new Project("5", "Title-5", "Description-5"));
        return items;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    protected Project(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
    }

    public static final Parcelable.Creator<Project> CREATOR = new Parcelable.Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel in) {
            return new Project(in);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };

}
