package id.koralin.android.labtek_indie.viewmodels;

import id.koralin.android.labtek_indie.models.User;

/**
 * Created by rfachrur on 02/10/17
 */

public class UserViewModel extends BaseViewModel
{
    private User mUser;

    public UserViewModel (User user)
    {
        mUser = user;
        notifyChange();
    }

    public void setUser (User user)
    {
        mUser = user;
        notifyChange();
    }

    public String getId ()
    {
        return String.valueOf(mUser.getId());
    }

    public String getName ()
    {
        return String.valueOf(mUser.getName());
    }

}
