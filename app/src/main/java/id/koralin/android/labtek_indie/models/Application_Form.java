package id.koralin.android.labtek_indie.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


public class Application_Form implements Parcelable {


    /**
     * status : OK
     * totalData : 1
     * data : [{"id_freelancer":"4","nama":"Bing","email":"bing@gmail.com","link_image":"","daily_rate":"999999","id_project":"2","client":"Labtek","durasi":"15","jenis":"2","total_sprint":"3","deskripsi":"Koralin Project for ADMIN","status_apply":"1","catatan":null}]
     */

    private String status;
    private int totalData;
    private List<DataBean> data;

    protected Application_Form(Parcel in) {
        status = in.readString();
        totalData = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeInt(totalData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Application_Form> CREATOR = new Creator<Application_Form>() {
        @Override
        public Application_Form createFromParcel(Parcel in) {
            return new Application_Form(in);
        }

        @Override
        public Application_Form[] newArray(int size) {
            return new Application_Form[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalData() {
        return totalData;
    }

    public void setTotalData(int totalData) {
        this.totalData = totalData;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id_freelancer : 4
         * nama : Bing
         * email : bing@gmail.com
         * link_image :
         * daily_rate : 999999
         * id_project : 2
         * client : Labtek
         * durasi : 15
         * jenis : 2
         * total_sprint : 3
         * deskripsi : Koralin Project for ADMIN
         * status_apply : 1
         * catatan : null
         */

        private String id_freelancer;
        private String nama;
        private String email;
        private String link_image;
        private String daily_rate;
        private String id_project;
        private String client;
        private String durasi;
        private String jenis;
        private String total_sprint;
        private String deskripsi;
        private String status_apply;
        private Object catatan;

        public String getId_freelancer() {
            return id_freelancer;
        }

        public void setId_freelancer(String id_freelancer) {
            this.id_freelancer = id_freelancer;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLink_image() {
            return link_image;
        }

        public void setLink_image(String link_image) {
            this.link_image = link_image;
        }

        public String getDaily_rate() {
            return daily_rate;
        }

        public void setDaily_rate(String daily_rate) {
            this.daily_rate = daily_rate;
        }

        public String getId_project() {
            return id_project;
        }

        public void setId_project(String id_project) {
            this.id_project = id_project;
        }

        public String getClient() {
            return client;
        }

        public void setClient(String client) {
            this.client = client;
        }

        public String getDurasi() {
            return durasi;
        }

        public void setDurasi(String durasi) {
            this.durasi = durasi;
        }

        public String getJenis() {
            return jenis;
        }

        public void setJenis(String jenis) {
            this.jenis = jenis;
        }

        public String getTotal_sprint() {
            return total_sprint;
        }

        public void setTotal_sprint(String total_sprint) {
            this.total_sprint = total_sprint;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getStatus_apply() {
            return status_apply;
        }

        public void setStatus_apply(String status_apply) {
            this.status_apply = status_apply;
        }

        public Object getCatatan() {
            return catatan;
        }

        public void setCatatan(Object catatan) {
            this.catatan = catatan;
        }
    }
}
