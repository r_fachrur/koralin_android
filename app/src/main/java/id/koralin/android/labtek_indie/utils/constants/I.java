package id.koralin.android.labtek_indie.utils.constants;

/**
 * Created by rfachrur on 02/11/17. integer constants
 */

public class I
{
    public static final int HTTP_NO_CONTENT       = 204;
    public static final int CAMERA_REQUEST_CODE   = 999;
    public static final int LOCATION_REQUEST_CODE = 998;
    public static final int SPLASH_DISPLAY_LENGTH = 500;
}
